# Perskit Piratenpartij Delft

Welkom bij de perskit van de [Piratenpartij Delft](https://delft.piratenpartij.nl). Hier bieden wij onder andere onze officiële logo's aan. De materialen zijn te downloaden in zwart-wit, kleur en inverse. Voor kleur hebben we aparte versies voor digitale publicaties (web) en voor drukwerk (print).

## Logo Piratenpartij Delft
<img src="/kleur/web/logo/ppdelft-logo-web.svg" width="50" />

Ons logo is hetzelfde als die van de Piratenpartij Nederland met als verschil de golflijntjes in de vlag. De golflijntjes zijn geïnspireerd op het wapen van Delft.

<img src="/kleur/web/logo-tagline-payoff/ppdelft-logo-tagline-payoff-web-paths.svg" width="300" />

In het samengestelde logo heeft de tagline Piratenpartij Delft het lettertype [Gehen Sans Bold](https://bbqsrc.github.io/gehen-fonts/) en heeft de pay-off het lettertype Gehen Sans. Indien de Gehen fonts mogelijk niet aanwezig zijn dient altijd het svg bestand gebaseerd op paths gekozen te worden.

## Kleuren Piratenpartij Delft
Alle kleurenversies van ons materiaal bevatten dezelfde basiskleur paars als die van de Piratenpartij Nederland. Voor drukwerk gebruiken we een paarstint met een wat hogere saturatie.

### Basiskleur digitale publicaties
![#572b85](https://via.placeholder.com/30/572b85/000000?text=+)

Hex: `#572b85`

RGB: `rgb(87,43,133)`

CMYK: `cmyk(35,68,0,48)`

HSV: `hsv(269.3,67.7,52.2)`

### Basiskleur drukwerk
![#511385](https://via.placeholder.com/30/511385/000000?text=+)

Hex: `#511385`

RGB: `rgb(81,19,133)`

CMYK: `cmyk(39,86,0,48)`

HSV: `hsv(272.6,85.7,52.2)`

### Licentie
Dit materiaal is bestemd voor het publieke domein en heeft daarom een CC0 licentie. Klik [hier](https://commons.wikimedia.org/wiki/File:Piratpartiet.svg#file) voor de licentie behorende bij het Piratenpartij logo en [hier](https://github.com/ppau/gehen-fonts/blob/master/LICENSE) voor de licenties behorende bij de Gehen fonts.